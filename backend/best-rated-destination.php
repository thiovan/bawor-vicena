<?php
require_once('config.php');

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=' . $maps_key . '&query=wisata%20banyumas&sensor=false&location=-7.454270,%20109.187804&radius=30000&type=point_of_interest&language=id');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
echo curl_exec($ch);

//dummy data for testing purpose
// $json = file_get_contents('json/best-rated-destination.json');
// echo $json;