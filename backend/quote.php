<?php

$quotes = [
    'Optimisme adalah keyakinan yang membawa pada keberhasilan. Tak ada yang bisa dilakukan tanpa harapan dan keyakinan',
    'Meningkatkan pada dasarnya merubah. Menjadi sempurna adalah perubahan yang dilakukan berulang-ulang',
    'Masa lalu memang tak dapat diubah. Tapi masa depan tergantung pada anda',
    'Jangan berusaha menjadi manusia yang sukses, tetapi jadilah manusia yang mempunyai nilai'
];

echo json_encode([
    'data'  => $quotes[rand(0, count($quotes) - 1)]
]);
