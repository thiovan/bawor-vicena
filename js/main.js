"use strict";

var deferredPrompt;

window.addEventListener("beforeinstallprompt", function (e) {
  e.preventDefault();
  deferredPrompt = e;

  console.log('before install');

  showAddToHomeScreen();
});

$(document).ready(function () {
  $("#content").load("screen/event.html");
  registerSW();
  //   greeting();
  //   initCarousel();
  //   getQuote();
});

function registerSW() {
  if ("serviceWorker" in navigator) {
    window.addEventListener('load', function () {
      navigator.serviceWorker.register("/sw.js").then(function () {
        console.log("Service Worker Registered");
      });
    });
  }
}

function showAddToHomeScreen() {
  var toastHTML =
    '<span>Tambahkan Bagor ke Home Screen</span><button id="btn-add-home" class="btn-flat toast-action">OKE</button>';
  M.toast({ html: toastHTML, displayLength: 8000 });
  $("#btn-add-home").click(function () {
    M.Toast.dismissAll();
    deferredPrompt.prompt();
    deferredPrompt.userChoice.then(function (choiceResult) {
      if (choiceResult.outcome === "accepted") {
        M.toast({ html: 'Bagor berhasil ditambahkan ke Home Screen' });
      }
      deferredPrompt = null;
    });
  });
}
