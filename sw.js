var cacheName = "bagor-0.0.1";
var filesToCache = [
  "/",
];
// var filesToCache = [
//   "/",
//   "/manifest.json",
//   "/index.html",
//   "/css/font/material-icons.woff2",
//   "/screen/home.html",
//   "/screen/destinasi.html",
//   "/css/materialize.min.css",
//   "/css/styles.css",
//   "/js/materialize.min.js",
//   "/js/main.js",
//   "/js/jquery.min.js",
//   "/js/moment.js",
//   "/images/icons/icon-72x72.png",
//   "/images/icons/icon-96x96.png",
//   "/images/icons/icon-128x128.png",
//   "/images/icons/icon-144x144.png",
//   "/images/icons/icon-152x152.png",
//   "/images/icons/icon-192x192.png",
//   "/images/icons/icon-384x384.png",
//   "/images/icons/icon-512x512.png",
//   "/images/clouded-moon.png",
//   "/images/appointment.png",
//   "/images/map-point.png",
//   "/images/traffic-lights.png",
//   "/images/nature.jfif",
//   "/images/nature2.jfif",
//   "/images/nature3.jfif"
// ];
let deferredPrompt;

self.addEventListener("install", function(e) {
  console.log("[ServiceWorker] Install");
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log("[ServiceWorker] Caching app shell");
      return cache.addAll(filesToCache);
    })
  );
});

self.addEventListener("activate", event => {
  event.waitUntil(self.clients.claim());
});

// self.addEventListener("fetch", event => {
//   event.respondWith(
//     caches.match(event.request, { ignoreSearch: true }).then(response => {
//       return response || fetch(event.request);
//     })
//   );
// });

self.addEventListener("fetch", function(event) {
  event.respondWith(
    caches.open(cacheName).then(function(cache) {
      return cache.match(event.request).then(function(response) {
        return (
          response ||
          fetch(event.request).then(function(response) {
            cache.put(event.request, response.clone());
            return response;
          })
        );
      });
    })
  );
});
